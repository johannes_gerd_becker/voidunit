/*
 (c) 2015 by Johannes Gerd Becker

 The code in this file, as well as the documentation, may be used under the terms of the
 Boost Software License Version 1.0, which you can find in voidunit.h.
*/

#include "voidunit.h"

#include <iostream>
#include <memory>
#include <string>

template<class F>
class MyTemplateClass {
public:
  // what the functional returns and what is to be stored (either the result type or the unit type)
  using ResultType = std::result_of_t <F()>;
  using StoredType = voidunit::target_type_t <ResultType>;

  static_assert (!std::is_reference<StoredType>::value, "That should not happen.");

  MyTemplateClass(F f) : m_f(std::move(f)) { }

  void CallAndStoreResult() {
    // Since reference wrappers cannot be default constructed, we cannot make m_stored a member
    // variable of type StoredType. In production code, we should use a boost::optional
    // rather than a std::unique_ptr, in order to avoid dynamic memory allocation.
    m_stored = std::make_unique<StoredType> ( voidunit::invoke_and_marshal(m_f /* , arguments */ ) );
  }

  ResultType RetrieveResult() {
    return voidunit::unmarshal<ResultType>(std::move(*m_stored));
  }

private:
  F m_f;
  std::unique_ptr<StoredType> m_stored;
};

template<class F> auto MakeMyTemplatedObject (F f) { return MyTemplateClass<F> (std::move (f)); }

int main() {
  auto t1 = MakeMyTemplatedObject([]() -> int {
    std::cout << "t1 called." << std::endl;
    return 42;
  });
  t1.CallAndStoreResult();
  auto t1result = t1.RetrieveResult();
  std::cout << "t1 returned " << t1result << "." << std::endl;

  auto t2 = MakeMyTemplatedObject([]() -> std::string {
    std::cout << "t2 called." << std::endl;
    return "best wishes";
  });
  t2.CallAndStoreResult();
  auto t2result = t2.RetrieveResult();
  std::cout << "t2 returned " << t2result << "." << std::endl;

  auto t3 = MakeMyTemplatedObject([]() -> void {
    std::cout << "t3 called. Will not return anything." << std::endl;
  });
  t3.CallAndStoreResult();
  t3.RetrieveResult(); /* does not do anything */

  auto t4 = MakeMyTemplatedObject([]() -> std::unique_ptr<int> {
    std::cout << "t4 called." << std::endl;
    return std::make_unique<int>(3);
  });
  t4.CallAndStoreResult();
  auto t4result = t4.RetrieveResult();
  std::cout << "t4 returned a unique pointer containing " << *t4result << "." << std::endl;

  int x = 999;
  auto t5 = MakeMyTemplatedObject([&x]() -> int & {
    std::cout << "t5 called." << std::endl;
    return x;
  });
  t5.CallAndStoreResult();
  auto & t5result = t5.RetrieveResult();
  x = 1001;
  std::cout << "t5 returned a reference containing now the value " << t5result << "." << std::endl;

  int u = 999;
  auto t6 = MakeMyTemplatedObject([&u]() -> int && {
    std::cout << "t6 called." << std::endl;
    return std::move(u);
  });
  t6.CallAndStoreResult();
  int && t6result = t6.RetrieveResult();
  u = 1002;
  std::cout << "t6 returned a reference containing now the value " << t6result << "." << std::endl;

}