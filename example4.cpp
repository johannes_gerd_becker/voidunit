#include <iostream>
#include "storage_box.h"

void demo1() {
  auto lambda = []() -> int { return 7; };
  auto box = voidunit::storage_box < std::result_of_t<decltype(lambda)()> >{};
  box.invoke_and_store_result(lambda);
  std::cout << box.retrieve_result() << std::endl;
}

void demo2() {
  int x = 10;
  auto lambda = [&x]() -> int & { return x; };
  auto box = voidunit::storage_box < std::result_of_t<decltype(lambda)()> >{};
  box.invoke_and_store_result(lambda);
  x = 7;
  std::cout << box.retrieve_result() << std::endl;
}

void demo3() {
  struct X { int x; };
  auto const x = X { 7 };
  auto lambda = [x]() -> X const { return x; };
  auto box = voidunit::storage_box < std::result_of_t<decltype(lambda)()> >{};
  box.invoke_and_store_result(lambda);
  std::cout << box.retrieve_result().x << std::endl;
}

void demo4() {
  auto lambda = []() -> void { throw 7; };
  auto box = voidunit::storage_box < std::result_of_t<decltype(lambda)()> >{};
  box.invoke_and_store_result(lambda);
  assert(box.has_exception());
  assert(!box.has_result());
  assert(box.was_called());
  try {
    box.retrieve_result();
    std::cout << "Something is wrong" << std::endl;
  } catch (int const & e) {
    std::cout << e << std::endl;
  }
}

int main() {
  demo1();
  demo2();
  demo3();
  demo4();
  return 0;
}