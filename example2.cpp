/*
 (c) 2015 by Johannes Gerd Becker

 The code in this file, as well as the documentation, may be used under the terms of the
 Boost Software License Version 1.0, which you can find in voidunit.h.
*/

#include "voidunit.h"

#include <iostream>
#include <memory>
#include <string>

template<class F>
class MyTemplateClass {
public:
  // what the functional returns and what is to be stored (either the result type or the unit type)
  using ResultType = std::result_of_t <F()>;
  using StoredType = voidunit::target_type_t <ResultType, voidunit::selector::rVoid>;

  static_assert (!std::is_reference<ResultType>::value, "Does this make sense?");
  static_assert (!std::is_reference<StoredType>::value, "Does this make sense?");

  MyTemplateClass(F f) : m_f(std::move(f)) { }

  void CallAndStoreResult() {
    m_stored = voidunit::invoke_and_marshal<voidunit::selector::rVoid>(m_f  /* , arguments */ );
  }

  ResultType RetrieveResult() {
    return voidunit::unmarshal<ResultType, voidunit::selector::rVoid>(std::move(m_stored));
  }

private:
  F m_f;
  StoredType m_stored;
};

template<class F> auto MakeMyTemplatedObject (F f) { return MyTemplateClass<F> (std::move (f)); }

int main() {
  auto t1 = MakeMyTemplatedObject([]() -> int {
    std::cout << "t1 called." << std::endl;
    return 42;
  });
  t1.CallAndStoreResult();
  auto t1result = t1.RetrieveResult();
  std::cout << "t1 returned " << t1result << "." << std::endl;

  auto t2 = MakeMyTemplatedObject([]() -> std::string {
    std::cout << "t2 called." << std::endl;
    return "best wishes";
  });
  t2.CallAndStoreResult();
  auto t2result = t2.RetrieveResult();
  std::cout << "t2 returned " << t2result << "." << std::endl;

  auto t3 = MakeMyTemplatedObject([]() -> void {
    std::cout << "t3 called. Will not return anything." << std::endl;
  });
  t3.CallAndStoreResult();
  t3.RetrieveResult(); /* does not do anything */

  auto t4 = MakeMyTemplatedObject([]() -> std::unique_ptr<int> {
    std::cout << "t4 called." << std::endl;
    return std::make_unique<int>(3);
  });
  t4.CallAndStoreResult();
  auto t4result = t4.RetrieveResult();
  std::cout << "t4 returned a unique pointer containing " << *t4result << "." << std::endl;
}