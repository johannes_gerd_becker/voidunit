/*
 (c) 2015 by Johannes Gerd Becker

 The code in this file, as well as the documentation, may be used under the terms of the
 Boost Software License Version 1.0, which you can find in voidunit.h.
*/

#include "voidunit.h"

#include <cassert>
#include <memory>
#include <type_traits>

template <class ResultType> struct Fct { ResultType operator () (); };

int main() {

  // no output, shows type conversions
  // for a runnable example see example2.cpp

  // double
  struct F { double operator () () {} };
  using G = double();
  static_assert (std::is_same <
    decltype(voidunit::invoke_and_marshal<voidunit::selector::rVoid>(Fct<double>{})), double
      > ::value, "Logic error: Unexpected result type."
  );

  // double &
  static_assert (std::is_same <
        decltype(
          voidunit::invoke_and_marshal<voidunit::selector::rVoid>(Fct<double &>{})), double &
          > ::value, "Logic error: Unexpected result type."
  );

  // double &&
  static_assert (std::is_same <
        decltype(voidunit::invoke_and_marshal<voidunit::selector::rVoid>(Fct<double &&>{})), double &&
      > ::value, "Logic error: Unexpected result type."
  );

  struct Something {};

  // Something
  static_assert (std::is_same <
    decltype(voidunit::invoke_and_marshal<voidunit::selector::rVoid>(Fct<Something>{})), Something
  > ::value, "Logic error: Unexpected result type."
    );

  // Something const
  static_assert (std::is_same <
    decltype(voidunit::invoke_and_marshal<voidunit::selector::rVoid>(Fct<Something const>{})), Something const
  > ::value, "Logic error: Unexpected result type."
    );

  // void
  static_assert (std::is_same <
    decltype(voidunit::invoke_and_marshal<voidunit::selector::rVoid>(Fct<void>{})),voidunit::unit_t>::value, "Logic error: Unexpected result type."
    );

  // unique_ptr
  auto res = voidunit::invoke_and_marshal<voidunit::selector::rVoid>([] { return std::make_unique<int>(222); });
  static_assert(std::is_same<decltype(res), std::unique_ptr<int>>::value, "Logic error: Unexpected result type.");
  int i = *res;
  assert(i == 222);

  // backwards conversion
  static_assert(std::is_same<decltype(voidunit::unmarshal<void,voidunit::selector::rVoid>(voidunit::unit_t{})), void>::value, "Logic error: Unexpected result type.");
  static_assert(std::is_same<decltype(voidunit::unmarshal<double, voidunit::selector::rVoid>(double{ 0 })), double>::value, "Logic error: Unexpected result type.");
  assert( (voidunit::unmarshal<double, voidunit::selector::rVoid>(double{ 42 }) == 42) );

  // invoke_and_marshal
  static_assert(std::is_same<decltype(voidunit::invoke_and_marshal(Fct<Something>{})), Something>::value, "Logic error: Unexpected result type");
  static_assert(std::is_same<decltype(voidunit::invoke_and_marshal(Fct<void>{})), voidunit::unit_t>::value, "Logic error: Unexpected result type");
  static_assert(std::is_same<decltype(voidunit::invoke_and_marshal(Fct<voidunit::unit_t>{})), voidunit::unit_t>::value, "Logic error: Unexpected result type");
  static_assert(std::is_same<decltype(voidunit::invoke_and_marshal(Fct<int &>{})), std::reference_wrapper<int>>::value, "Logic error: Unexpected result type");
  static_assert(std::is_same<decltype(voidunit::invoke_and_marshal(Fct<int const &>{})), std::reference_wrapper<int const>>::value, "Logic error: Unexpected result type");
  static_assert(std::is_same<decltype(voidunit::invoke_and_marshal(Fct<int &&>{})), voidunit::rvalue_reference_wrapper<int>>::value, "Logic error: Unexpected result type");

  static_assert(std::is_same<decltype(voidunit::unmarshal<void>(std::declval<voidunit::unit_t>())), void>::value, "Logic error:Unexpected result type.");
  static_assert(std::is_same<decltype(voidunit::unmarshal<voidunit::unit_t>(std::declval<voidunit::unit_t>())), voidunit::unit_t>::value, "Logic error:Unexpected result type.");
  static_assert(std::is_same<decltype(voidunit::unmarshal<int>(std::declval<int>())), int>::value, "Logic error:Unexpected result type.");
  static_assert(std::is_same<decltype(voidunit::unmarshal<int &>(std::declval<std::reference_wrapper<int>>())),int &>::value, "Logic error:Unexpected result type.");
  static_assert(std::is_same<decltype(voidunit::unmarshal<int &&>(std::declval<voidunit::rvalue_reference_wrapper<int>>())), int &&>::value, "Logic error:Unexpected result type.");
}