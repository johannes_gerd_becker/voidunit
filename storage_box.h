#pragma once

#include <exception>
#include <type_traits>
#include <utility>
#include "boost/optional.hpp"
#include "voidunit.h"

namespace voidunit {

  template<class ResultType>
  class storage_box {
  public:
    using result_type = ResultType;

  private:
    using stored_type = std::remove_const_t< storable_t <result_type> >;
    static_assert (!std::is_reference<stored_type>::value, "That should not happen.");

  public:
    template <class F, class ... Args>
    void invoke_and_store_result(F && f, Args && ... args) {
      m_stored = boost::none;
      m_eptr = nullptr;
      try {
        m_stored = storable_invoke(std::forward<F> (f), std::forward<Args>(args) ...);
      } catch (...) {
        m_eptr = std::current_exception();
      }
    }

    bool has_exception() const { return !!m_eptr; }
    bool has_result() const { return !!m_stored; }
    bool was_called() const { return has_result() || has_exception(); }

    result_type retrieve_result() {
      using std::swap;
      if (m_eptr) {
        auto eptr = std::exception_ptr{};
        swap(m_eptr, eptr);
        std::rethrow_exception(eptr);
        throw 0; /* to avoid compiler warning about not returning a value */
      } else {
        auto result = boost::optional<stored_type>{};
        swap(m_stored, result);
        return restore_from_storable<result_type>(std::move(*result));
      }
    }

  private:
    boost::optional<stored_type> m_stored;
    std::exception_ptr m_eptr;
  };

}